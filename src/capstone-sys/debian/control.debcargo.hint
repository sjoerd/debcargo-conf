Source: rust-capstone-sys
Section: rust
Priority: optional
Build-Depends: debhelper (>= 12),
 dh-cargo (>= 24),
 cargo:native <!nocheck>,
 rustc:native <!nocheck>,
 libstd-rust-dev <!nocheck>,
 librust-cc-1+default-dev <!nocheck>,
 librust-libc-0.2-dev (>= 0.2.59-~~) <!nocheck>
Maintainer: Debian Rust Maintainers <pkg-rust-maintainers@alioth-lists.debian.net>
Uploaders:
 Michael R. Crusoe <crusoe@debian.org>
Standards-Version: 4.5.1
Vcs-Git: https://salsa.debian.org/rust-team/debcargo-conf.git [src/capstone-sys]
Vcs-Browser: https://salsa.debian.org/rust-team/debcargo-conf/tree/master/src/capstone-sys
Rules-Requires-Root: no

Package: librust-capstone-sys-dev
Architecture: any
Multi-Arch: same
Depends:
 ${misc:Depends},
 librust-cc-1+default-dev,
 librust-libc-0.2-dev (>= 0.2.59-~~)
Suggests:
 librust-capstone-sys+bindgen-dev (= ${binary:Version}),
 librust-capstone-sys+regex-dev (= ${binary:Version}),
 librust-capstone-sys+use-bindgen-dev (= ${binary:Version})
Provides:
 librust-capstone-sys+default-dev (= ${binary:Version}),
 librust-capstone-sys-0-dev (= ${binary:Version}),
 librust-capstone-sys-0+default-dev (= ${binary:Version}),
 librust-capstone-sys-0.11-dev (= ${binary:Version}),
 librust-capstone-sys-0.11+default-dev (= ${binary:Version}),
 librust-capstone-sys-0.11.0-dev (= ${binary:Version}),
 librust-capstone-sys-0.11.0+default-dev (= ${binary:Version})
Description: System bindings to the capstone disassembly library - Rust source code
 This package contains the source for the Rust capstone-sys crate, packaged by
 debcargo for use with cargo and dh-cargo.

Package: librust-capstone-sys+bindgen-dev
Architecture: any
Multi-Arch: same
Depends:
 ${misc:Depends},
 librust-capstone-sys-dev (= ${binary:Version}),
 librust-bindgen-0.53+default-dev
Provides:
 librust-capstone-sys-0+bindgen-dev (= ${binary:Version}),
 librust-capstone-sys-0.11+bindgen-dev (= ${binary:Version}),
 librust-capstone-sys-0.11.0+bindgen-dev (= ${binary:Version})
Description: System bindings to the capstone disassembly library - feature "bindgen"
 This metapackage enables feature "bindgen" for the Rust capstone-sys crate, by
 pulling in any additional dependencies needed by that feature.

Package: librust-capstone-sys+regex-dev
Architecture: any
Multi-Arch: same
Depends:
 ${misc:Depends},
 librust-capstone-sys-dev (= ${binary:Version}),
 librust-regex-1+default-dev (>= 1.3.1-~~)
Provides:
 librust-capstone-sys-0+regex-dev (= ${binary:Version}),
 librust-capstone-sys-0.11+regex-dev (= ${binary:Version}),
 librust-capstone-sys-0.11.0+regex-dev (= ${binary:Version})
Description: System bindings to the capstone disassembly library - feature "regex"
 This metapackage enables feature "regex" for the Rust capstone-sys crate, by
 pulling in any additional dependencies needed by that feature.

Package: librust-capstone-sys+use-bindgen-dev
Architecture: any
Multi-Arch: same
Depends:
 ${misc:Depends},
 librust-capstone-sys-dev (= ${binary:Version}),
 librust-bindgen-0.53+default-dev,
 librust-regex-1+default-dev (>= 1.3.1-~~)
Provides:
 librust-capstone-sys-0+use-bindgen-dev (= ${binary:Version}),
 librust-capstone-sys-0.11+use-bindgen-dev (= ${binary:Version}),
 librust-capstone-sys-0.11.0+use-bindgen-dev (= ${binary:Version})
Description: System bindings to the capstone disassembly library - feature "use_bindgen"
 This metapackage enables feature "use_bindgen" for the Rust capstone-sys crate,
 by pulling in any additional dependencies needed by that feature.
