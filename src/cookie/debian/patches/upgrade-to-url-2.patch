From 024ac8e9d10d195139e41b3fd97ad5222c5fb05c Mon Sep 17 00:00:00 2001
From: Nikhil Benesch <nikhil.benesch@gmail.com>
Date: Tue, 30 Jul 2019 11:16:41 -0400
Subject: [PATCH] Upgrade to url 2.0 ecosystem

The url crate recently released a backwards-incompatible update, v2.0.
As part of this upgrade, the percent-encoding code has been moved into
its own crate, titled percent-encoding, and no longer ships standard
percent encoding sets.
---
 Cargo.toml   |  4 ++--
 src/lib.rs   | 26 ++++++++++++++++++++++++--
 src/parse.rs |  2 +-
 3 files changed, 27 insertions(+), 5 deletions(-)

diff --git a/Cargo.toml b/Cargo.toml
index 61f0adae..8a4be92a 100644
--- a/Cargo.toml
+++ b/Cargo.toml
@@ -32,9 +32,13 @@
 version = "0.1"
 
 [dependencies.url]
-version = "1.0"
+version = "2"
+optional = true
+
+[dependencies.percent-encoding]
+version = "2"
 optional = true
 
 [features]
-percent-encode = ["url"]
+percent-encode = ["percent-encoding"]
 secure = ["ring", "base64"]
diff --git a/src/lib.rs b/src/lib.rs
index c4aab99d..dc32fed1 100644
--- a/src/lib.rs
+++ b/src/lib.rs
@@ -61,7 +61,7 @@
 #![doc(html_root_url = "https://docs.rs/cookie/0.12")]
 #![deny(missing_docs)]
 
-#[cfg(feature = "percent-encode")] extern crate url;
+#[cfg(feature = "percent-encode")] extern crate percent_encoding;
 extern crate time;
 
 mod builder;
@@ -81,7 +81,7 @@ use std::str::FromStr;
 use std::ascii::AsciiExt;
 
 #[cfg(feature = "percent-encode")]
-use url::percent_encoding::{USERINFO_ENCODE_SET, percent_encode};
+use percent_encoding::{AsciiSet, percent_encode};
 use time::{Tm, Duration};
 
 use parse::parse_cookie;
@@ -905,6 +905,28 @@ impl<'c> Cookie<'c> {
     }
 }
 
+/// https://url.spec.whatwg.org/#fragment-percent-encode-set
+#[cfg(feature = "percent-encode")]
+const FRAGMENT_ENCODE_SET: &AsciiSet = &percent_encoding::CONTROLS.add(b' ').add(b'"').add(b'<').add(b'>').add(b'`');
+
+/// https://url.spec.whatwg.org/#path-percent-encode-set
+#[cfg(feature = "percent-encode")]
+const PATH_ENCODE_SET: &AsciiSet = &FRAGMENT_ENCODE_SET.add(b'#').add(b'?').add(b'{').add(b'}');
+
+/// https://url.spec.whatwg.org/#userinfo-percent-encode-set
+#[cfg(feature = "percent-encode")]
+const USERINFO_ENCODE_SET: &AsciiSet = &PATH_ENCODE_SET
+    .add(b'/')
+    .add(b':')
+    .add(b';')
+    .add(b'=')
+    .add(b'@')
+    .add(b'[')
+    .add(b'\\')
+    .add(b']')
+    .add(b'^')
+    .add(b'|');
+
 /// Wrapper around `Cookie` whose `Display` implementation percent-encodes the
 /// cookie's name and value.
 ///
diff --git a/src/parse.rs b/src/parse.rs
index be0a2227..f224a8c7 100644
--- a/src/parse.rs
+++ b/src/parse.rs
@@ -9,7 +9,7 @@ use std::convert::From;
 use std::ascii::AsciiExt;
 
 #[cfg(feature = "percent-encode")]
-use url::percent_encoding::percent_decode;
+use percent_encoding::percent_decode;
 use time::{self, Duration};
 
 use ::{Cookie, SameSite, CookieStr};
